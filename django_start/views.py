import string

from django.http import HttpResponse

# Create your views here.
from django_start.db import execute_query
from django_start.utils import generate_password, render_list
from django_start.validation import get_integer_from_arguments


def main(request):
    result = HttpResponse("OK")
    result.headers["Content-Type"] = 'text/plain'
    return result


def get_random(request):
    try:
        length = get_integer_from_arguments(
            request.GET, 'length', '10', 1, 110)
        count = get_integer_from_arguments(
            request.GET, 'count', min_=1, max_=10)
    except ValueError as e:
        return HttpResponse(str(e), status=400)

    chars = string.ascii_lowercase + string.ascii_uppercase
    passwords = []
    for i in range(count):
        passwords.append(generate_password(chars, length))

    response = HttpResponse("\n".join(passwords))
    response.headers['Content-Type'] = 'text/plain'
    return response


def get_customers(request):
    country = request.GET['country']

    result = execute_query("""SELECT FirstName, LastName, Country, City 
                              FROM Customers
                              WHERE Country = ?""", (country,))
    return render_list(result)
