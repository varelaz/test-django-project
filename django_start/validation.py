def get_integer_from_arguments(args, name, default=None, min_=None, max_=None):
    value = args.get(name, default)
    arg_name = name.title()

    if value is None:
        raise ValueError(f"Error. {arg_name} is required")

    try:
        value = int(value)
    except ValueError:
        raise ValueError(f"Error. {arg_name} is not integer")

    if min_ is not None:
        if value < min_:
            raise ValueError(
                f"Error. {arg_name} has to be more or equal to {min_}"
            )

    if max_ is not None:
        if value > max_:
            raise ValueError(
                f"Error. {arg_name} has to be less or equal to {max_}"
            )

    return value
